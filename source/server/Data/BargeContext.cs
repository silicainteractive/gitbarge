using GitBarge.Lib.Models;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace GitBarge.Server.Data {
    public class BargeContext : DbContext
    {
        public DbSet<Repository> Repositories {get;set;}

        public BargeContext(DbContextOptions options) : base(options)
        {
        }
    }
}