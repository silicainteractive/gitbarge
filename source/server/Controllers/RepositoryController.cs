using System;
using System.Threading.Tasks;
using GitBarge.Lib.Models;
using GitBarge.Server.Data;
using Microsoft.AspNetCore.Mvc;

namespace GitBarge.Server.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class RepositoryController : Controller {
        private readonly BargeContext context;

        public RepositoryController(BargeContext context){
            this.context = context;
        }

        [HttpPost()]
        public async Task<IActionResult> RegisterAsync([FromBody] Repository input)
        {
            await context.Repositories.AddAsync(input);
            await context.SaveChangesAsync();

            return Ok();
        }

        public IActionResult Get()
            => new OkObjectResult(context.Repositories);
    }
}