using System;

namespace GitBarge.Lib.Models {
    public class Repository
    {
        public Guid Id {get;set;}
        public string Path { get; set; }
    }
}