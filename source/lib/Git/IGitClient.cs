namespace GitBarge.Lib.Git {
    public interface IGitClient
    {
        string GetRepositoryBasePath(string path);
        
    }
}