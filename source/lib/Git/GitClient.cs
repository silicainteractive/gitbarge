using LibGit2Sharp;

namespace GitBarge.Lib.Git {
    // NOT COVERED BY UNIT TESTS
    public class GitClient : IGitClient
    {
        public string GetRepositoryBasePath(string path)
            => Repository.Discover(path);
    }
}