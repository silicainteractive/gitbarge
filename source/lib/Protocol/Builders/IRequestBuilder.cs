using GitBarge.Lib.Protocol.Builders.Repository;

namespace GitBarge.Lib.Protocol.Builders {
    public interface IRequestBuilder
    {
        RepositoryCommandBuilder Repository();
    }
}