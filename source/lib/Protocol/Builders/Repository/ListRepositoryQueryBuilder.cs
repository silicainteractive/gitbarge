using GitBarge.Lib.Protocol.Queries;

namespace GitBarge.Lib.Protocol.Builders.Repository {
    public class ListRepositoryQueryBuilder {
        public ListRepositoryQuery Build()
            => new ListRepositoryQuery();
    }
}