using System;
using GitBarge.Lib.Exceptions;
using GitBarge.Lib.Git;
using GitBarge.Lib.Protocol.Commands;

namespace GitBarge.Lib.Protocol.Builders.Repository {
    public class RegisterRepositoryCommandBuilder
    {
        private RegisterRepositoryCommand command;
        private IGitClient gitClient;

        public RegisterRepositoryCommandBuilder(IGitClient gitClient)
        {
            command = new RegisterRepositoryCommand();
            this.gitClient = gitClient;
        }

        public RegisterRepositoryCommandBuilder WithPath(string path)
        {
            var basePath = gitClient.GetRepositoryBasePath(path);
            if(basePath == null)
                throw new RepositoryInvalidException($"Could not find repository with path: {path}");
    	    command.Model.Path = basePath;
            return this;
        }

        public RegisterRepositoryCommand Build()
            => command;
    }
}