using System;
using GitBarge.Lib.Git;

namespace GitBarge.Lib.Protocol.Builders.Repository
{
    public class RepositoryCommandBuilder
    {
        private readonly IGitClient gitClient;

        public RepositoryCommandBuilder(IGitClient gitClient){
            this.gitClient = gitClient;
        }
        public RegisterRepositoryCommandBuilder Register()
            => new RegisterRepositoryCommandBuilder(gitClient);
        
        public ListRepositoryQueryBuilder List()
            => new ListRepositoryQueryBuilder();
    }
}