using GitBarge.Lib.Git;
using GitBarge.Lib.Protocol.Builders.Repository;

namespace GitBarge.Lib.Protocol.Builders {
    public class RequestBuilder : IRequestBuilder
    {
        private readonly IGitClient gitClient;

        public RequestBuilder(IGitClient gitClient){
            this.gitClient = gitClient;
        }
        public RepositoryCommandBuilder Repository()
            => new RepositoryCommandBuilder(gitClient);
    }
}