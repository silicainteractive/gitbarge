using System.Net.Http;
using System.Threading.Tasks;

namespace GitBarge.Lib.Protocol.Clients {
    //NOT UNIT TESTED
    public class BargeClient : IBargeClient
    {
        private readonly HttpClient httpClient;

        public BargeClient(HttpClient httpClient){
            this.httpClient = httpClient;
        }

        public async Task<HttpResponseMessage> GetAsync(string endpoint)
            => await httpClient.GetAsync(endpoint);

        public async Task<HttpResponseMessage> PostAsync(string endpoint, StringContent content)
            => await httpClient.PostAsync(endpoint, content);
    }
}