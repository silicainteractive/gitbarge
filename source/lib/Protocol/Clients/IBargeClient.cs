using System.Threading.Tasks;
using System.Net.Http;
using GitBarge.Lib.Protocol.Commands;

namespace GitBarge.Lib.Protocol.Clients {
    public interface IBargeClient
    {
        Task<HttpResponseMessage> PostAsync(string endpoint, StringContent content);
        Task<HttpResponseMessage> GetAsync(string endpoint);
    }
}