using System.Net.Http;
using System.Text;
using GitBarge.Lib.Protocol.Clients;
using GitBarge.Lib.Protocol.Commands;
using GitBarge.Lib.Protocol.Queries;
using Newtonsoft.Json;

namespace GitBarge.Lib.Protocol.Services {
    public class BargeService : IBargeService
    {
        private readonly IBargeClient client;

        public BargeService(IBargeClient client){
            this.client = client;
        }

        public StatusCode ExecuteCommand<T>(ICommand<T> command)
        {
            var body = JsonConvert.SerializeObject(command.Model);
            var content = new StringContent(body, Encoding.UTF8, "application/json");

            var response = client.PostAsync(command.Endpoint, content).Result;

            if(response.IsSuccessStatusCode){
                return StatusCode.OK;
            }

            return StatusCode.Error;
            
        }

        public QueryResult<T> ExecuteQuery<T>(IQuery<T> query)
        {
            var response = client.GetAsync(query.Endpoint).Result;
            if(!response.IsSuccessStatusCode)
                return new QueryResult<T>{
                    StatusCode = StatusCode.Error
                };

            var json = response.Content.ReadAsStringAsync().Result; 
            return new QueryResult<T>{
                StatusCode =  StatusCode.OK,
                Model = JsonConvert.DeserializeObject<T>(json)
            };
        }
    }
}