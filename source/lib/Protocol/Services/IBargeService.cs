using GitBarge.Lib.Protocol.Commands;
using GitBarge.Lib.Protocol.Queries;

namespace GitBarge.Lib.Protocol.Services {
    public interface IBargeService
    {
        StatusCode ExecuteCommand<T>(ICommand<T> command);
        QueryResult<T> ExecuteQuery<T>(IQuery<T> command);
    }
}