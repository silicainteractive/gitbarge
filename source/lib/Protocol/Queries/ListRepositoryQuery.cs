using System.Collections.Generic;
using GitBarge.Lib.Models;

namespace GitBarge.Lib.Protocol.Queries {
    public class ListRepositoryQuery : IQuery<IEnumerable<Repository>>
    {
        public string Endpoint => "api/Repository";
    }
}