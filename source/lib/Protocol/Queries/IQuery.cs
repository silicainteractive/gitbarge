namespace GitBarge.Lib.Protocol.Queries {
    public interface IQuery<T>
    {
        string Endpoint {get;}
    }
}