namespace GitBarge.Lib.Protocol.Queries {
    public class QueryResult<T> {
        public T Model {get; internal set;}
        public StatusCode StatusCode {get; internal set;}
    }
}