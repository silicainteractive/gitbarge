using GitBarge.Lib.Models;

namespace GitBarge.Lib.Protocol.Commands {
    public class RegisterRepositoryCommand : ICommand<Repository>{
        public string Endpoint => "/api/Repository";
        public Repository Model {get; private set;}

        public RegisterRepositoryCommand(){
            Model = new Repository();
        }
    }
}