namespace GitBarge.Lib.Protocol.Commands {
    public interface ICommand<T>
    {
        string Endpoint {get;}
        T Model {get;}
    }
}