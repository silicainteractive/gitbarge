using System;

namespace GitBarge.Lib.Exceptions {
    [Serializable]
    public class RepositoryInvalidException : Exception{
        public RepositoryInvalidException() { }
        public RepositoryInvalidException(string message) : base(message) { }
        public RepositoryInvalidException(string message, System.Exception inner) : base(message, inner) { }
        protected RepositoryInvalidException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}