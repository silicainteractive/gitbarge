using System;
using GitBarge.Lib.Protocol;
using GitBarge.Lib.Protocol.Commands;
using GitBarge.Lib.Exceptions;
using McMaster.Extensions.CommandLineUtils;
using GitBarge.Lib.Protocol.Builders;
using GitBarge.Lib.Protocol.Services;
using System.Linq;
using System.IO;
using System.Runtime.CompilerServices;

namespace GitBarge.Client {
    public class Runner
    {
        private IBargeService service;
        private IRequestBuilder commandBuilder;

        public Runner(IBargeService service, IRequestBuilder commandBuilder)
        {
            this.service = service;
            this.commandBuilder = commandBuilder;
        }

        public int Run(string[] args)
        {
            var app = new CommandLineApplication{
                Name = "git-barge",
                Description = "Find your friends!"
            };
            app.HelpOption(inherited: true);

            app.Command("repository", repCmd => {
                var repoCommandBuilder = commandBuilder.Repository();
                repCmd.OnExecute( () => {
                    Console.WriteLine("not implemented");
                    return 1;
                });

                repCmd.Command("register", regCmd => {
                    var regCommandBuilder = repoCommandBuilder.Register();
                    var path = regCmd.Argument("path", "path of the repository").IsRequired();
                    regCmd.OnExecute(() => {
                        try{
                        var command = regCommandBuilder.WithPath(path.Value).Build();
                        var exitCode = (int) service.ExecuteCommand(command);
                        return exitCode;
                        } catch (RepositoryInvalidException ex){
                            Console.WriteLine(ex.Message);
                            return (int) StatusCode.Error;
                        }
                    });
                });

                repCmd.Command("list", listCmd => {
                    var listCommandBuilder = repoCommandBuilder.List();
                    listCmd.OnExecute(() => {
                        var query = listCommandBuilder.Build();
                        var result = service.ExecuteQuery(query);
                        if(result.StatusCode == StatusCode.OK){
                            Console.WriteLine($"{result.Model.Count()} repositories found:");
                            foreach(var m  in result.Model){
                                Console.WriteLine(m.Path);
                            }
                        }
                        return (int) result.StatusCode;
                    });
                });
            });

            app.OnExecute(() =>
            {
                Console.WriteLine("Specify a subcommand");
                app.ShowHelp();
                return 1;
            });

            return app.Execute(args);
        }
    }
}