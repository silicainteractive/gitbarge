using System;
using FluentAssertions;
using GitBarge.Lib.Protocol;
using GitBarge.Lib.Protocol.Commands;
using Moq;
using Xunit;
using GitBarge.Lib.Git;
using GitBarge.Lib.Exceptions;
using GitBarge.Lib.Protocol.Builders;
using GitBarge.Lib.Protocol.Services;
using GitBarge.Lib.Protocol.Builders.Repository;

namespace GitBarge.Client.Tests
{
    public class RunnerTests
    {
        [Fact]
        public void RunnerReturnsStatusOk()
        {
            var pathArg = "./path/";
            var serviceMock = new Mock<IBargeService>();
            serviceMock
                .Setup(m => m.ExecuteCommand(It.Is<RegisterRepositoryCommand>(c => c.Model.Path == pathArg)))
                .Returns(StatusCode.OK)
                .Verifiable();

            var builderMock = new Mock<IRequestBuilder>();
            var mock = new Mock<IGitClient>();
            mock.Setup(m => m.GetRepositoryBasePath(It.Is<string>(s => s == pathArg))).Returns(pathArg);

            builderMock.Setup(m => m.Repository()).Returns(new RepositoryCommandBuilder(mock.Object)).Verifiable();
            var runner = new Runner(serviceMock.Object, builderMock.Object);
            
            var exitCode = runner.Run(new [] {"repository", "register", pathArg});

            exitCode.Should().Be(0);
            serviceMock.VerifyAll();
            builderMock.VerifyAll();
        }
        [Fact]
        public void RunnerReturnsStatusError(){
            var pathArg = "./path/";
            var serviceMock = new Mock<IBargeService>();

            var builderMock = new Mock<IRequestBuilder>();
            var mock = new Mock<IGitClient>();
            mock.Setup(m => m.GetRepositoryBasePath(It.Is<string>(s => s == pathArg))).Throws(new RepositoryInvalidException());

            builderMock.Setup(m => m.Repository()).Returns(new RepositoryCommandBuilder(mock.Object)).Verifiable();
            var runner = new Runner(serviceMock.Object, builderMock.Object);
            
            var exitCode = runner.Run(new [] {"repository", "register", pathArg});

            exitCode.Should().Be(1);
            builderMock.VerifyAll();
        }
    }
}
