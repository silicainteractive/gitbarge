using System;
using System.Net;
using System.Net.Http;
using System.Text;
using FluentAssertions;
using GitBarge.Lib.Models;
using GitBarge.Lib.Protocol.Clients;
using GitBarge.Lib.Protocol.Commands;
using GitBarge.Lib.Protocol.Queries;
using GitBarge.Lib.Protocol.Services;
using Moq;
using Newtonsoft.Json;
using Xunit;

namespace GitBarge.Lib.Protocol.Tests {
    public class BargeServiceTests {
        [Fact]
        public void ExecuteCommandHappyFlow(){
            var clientMock = new Mock<IBargeClient>();
            var endpoint = "/endpoint";
            clientMock
                .Setup(m => m.PostAsync(It.Is<string>(s => s == endpoint), It.IsAny<StringContent>()))
                .ReturnsAsync(new HttpResponseMessage(HttpStatusCode.OK))
                .Verifiable();
            
            var commandMock = new Mock<ICommand<Repository>>();
            commandMock.Setup(m => m.Endpoint).Returns(endpoint);
            
            var service = new BargeService(clientMock.Object);
            service.Should().BeAssignableTo<IBargeService>();
            
            var statusCode = service.ExecuteCommand(commandMock.Object);

            statusCode.Should().Be(StatusCode.OK);

            clientMock.VerifyAll();
        }
        [Fact]
        public void ExecuteCommandReturnsStatusCodeError(){
            var clientMock = new Mock<IBargeClient>();
            var endpoint = "/endpoint";
            clientMock
                .Setup(m => m.PostAsync(It.Is<string>(s => s == endpoint), It.IsAny<StringContent>()))
                .ReturnsAsync(new HttpResponseMessage(HttpStatusCode.BadRequest))
                .Verifiable();
            
            var commandMock = new Mock<ICommand<Repository>>();
            commandMock.Setup(m => m.Endpoint).Returns(endpoint);
            
            var service = new BargeService(clientMock.Object);
            
            var statusCode = service.ExecuteCommand(commandMock.Object);

            statusCode.Should().Be(StatusCode.Error);

            clientMock.VerifyAll();
        }

        [Fact]
        public void ExecuteQueryHappyFlow(){
            var clientMock = new Mock<IBargeClient>();
            var endpoint = "/endpoint";
            var expectedResult = new Repository {
                Id = Guid.NewGuid(),
                Path = "./path"
            };
            var json = JsonConvert.SerializeObject(expectedResult);
            clientMock
                .Setup(m => m.GetAsync(It.Is<string>(s => s == endpoint)))
                .ReturnsAsync(new HttpResponseMessage{
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(json, Encoding.UTF8, "application/json")
                }).Verifiable();
            
            var queryMock = new Mock<IQuery<Repository>>();
            queryMock.Setup(m => m.Endpoint).Returns(endpoint);
            
            var service = new BargeService(clientMock.Object);
            
            var actualResult = service.ExecuteQuery(queryMock.Object);
            actualResult.StatusCode.Should().Be(StatusCode.OK);

            actualResult.Model.Id.Should().Be(expectedResult.Id);
            actualResult.Model.Path.Should().Be(expectedResult.Path);

            clientMock.VerifyAll();
        }

        [Fact]
        public void ExecuteQueryReturnsStatusCodeError(){
            var clientMock = new Mock<IBargeClient>();
            var endpoint = "/endpoint";
            var expectedResult = new Repository {
                Id = Guid.NewGuid(),
                Path = "./path"
            };
            var json = JsonConvert.SerializeObject(expectedResult);
            clientMock
                .Setup(m => m.GetAsync(It.Is<string>(s => s == endpoint)))
                .ReturnsAsync(new HttpResponseMessage{
                    StatusCode = HttpStatusCode.BadRequest,
                }).Verifiable();
            
            var queryMock = new Mock<IQuery<Repository>>();
            queryMock.Setup(m => m.Endpoint).Returns(endpoint);
            
            var service = new BargeService(clientMock.Object);
            
            var actualResult = service.ExecuteQuery(queryMock.Object);
            actualResult.StatusCode.Should().Be(StatusCode.Error);
            actualResult.Model.Should().BeNull();

            clientMock.VerifyAll();
        }
    }
}