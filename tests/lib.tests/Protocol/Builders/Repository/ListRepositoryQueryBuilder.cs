using System.Collections.Generic;
using FluentAssertions;
using GitBarge.Lib.Protocol.Builders.Repository;
using GitBarge.Lib.Protocol.Queries;
using Xunit;

namespace GitBarge.Lib.Protocol.Builders.Repository.Tests {
    public class ListRepositoryQueryBuilderTests {
        [Fact]
        public void ListReturnsQuery(){
            var query = new ListRepositoryQueryBuilder().Build();
            query.Should().BeAssignableTo<IQuery<IEnumerable<GitBarge.Lib.Models.Repository>>>();
        }
    }
}