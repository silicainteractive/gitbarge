using System;
using FluentAssertions;
using GitBarge.Lib.Exceptions;
using GitBarge.Lib.Git;
using GitBarge.Lib.Protocol.Builders.Repository;
using GitBarge.Lib.Protocol.Commands;
using Moq;
using Xunit;

namespace GitBarge.Lib.Protocol.Builders.Repository.Tests {
    public class RepositoryCommandBuilderTests {

        [Fact]
        public void RegisterReturnsRegisterBuilder(){
            var mock = new Mock<IGitClient>();
            var builder = new RepositoryCommandBuilder(mock.Object);
            var rBuilder = builder.Register();
            rBuilder.Should().BeOfType<RegisterRepositoryCommandBuilder>();
        }

        [Fact]
        public void RegisterWithPathShouldFailOnInvalidRepo(){
            var path = "./path/";
            var mock = new Mock<IGitClient>();
            mock.Setup(m => m.GetRepositoryBasePath(It.Is<string>(s => s == path)))
                .Throws(new RepositoryInvalidException())
                .Verifiable();

            var builder = new RegisterRepositoryCommandBuilder(mock.Object);
            Action action = () => builder.WithPath(path);
            
            action.Should().Throw<RepositoryInvalidException>();
            mock.VerifyAll();
        }
    }
}