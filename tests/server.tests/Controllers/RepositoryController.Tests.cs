using System.Threading.Tasks;
using GitBarge.Lib.Models;
using GitBarge.Server.Data;
using Microsoft.EntityFrameworkCore;
using Xunit;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace GitBarge.Server.Controllers.Tests {
    public class RepositoryControllerTests {
        [Fact]
        public async Task RegisterAddsRepositoryToDb(){
            var contextOptions = new DbContextOptionsBuilder<BargeContext>()
                .UseInMemoryDatabase(databaseName: "post")
                .Options;
            var context = new BargeContext(contextOptions);
            var input = new Repository{
                Path = "./path/"
            };

            var sut = new RepositoryController(context);

            var postResult = (await sut.RegisterAsync(input)) as OkResult;
            postResult.Should().NotBeNull();

            context.Repositories.Should().Contain(input);
        }
        [Fact]
        public void GetRetrievesRepositoriesFromDb(){
            var contextOptions = new DbContextOptionsBuilder<BargeContext>()
                .UseInMemoryDatabase(databaseName: "get")
                .Options;
            var context = new BargeContext(contextOptions);
            var input =  new Repository{
                Path = "./path/"
            };
            context.Repositories.Add(input);
            context.SaveChanges();

            var sut = new RepositoryController(context);
            var result = (sut.Get() as OkObjectResult);
            var value = (result.Value as IEnumerable<Repository>);

            value.Should().Contain(input);
            
        }
    }
}