using System;
using Xunit;
using Xbehave;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc.Testing;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using GitBarge.Lib.Models;
using GitBarge.Lib.Protocol;
using Microsoft.Extensions.DependencyInjection;
using GitBarge.Lib.Git;
using Moq;
using GitBarge.Lib.Protocol.Clients;
using GitBarge.Lib.Protocol.Builders;
using GitBarge.Lib.Protocol.Services;
using System.IO;

namespace GitBarge.Specifications
{
    public class RegistrationFeature : IClassFixture<SpecWebApplicationFactory>
    {
        private readonly SpecWebApplicationFactory factory;
        private readonly Mock<IGitClient> gitClient;
        public RegistrationFeature(SpecWebApplicationFactory factory){
            this.factory = factory;
            this.gitClient = new Mock<IGitClient>();
        }

        [Scenario]
        [Example("/Repository")]
        public void EndpointTests(string endpoint, HttpClient client, HttpResponseMessage response){
            "Given we have a server"
                .x(() => client = factory.CreateClient());
            $"When we ask the server for a response on {endpoint}"
                .x(async () => response = await client.GetAsync($"/api{endpoint}"));
            "Then the response should be successful"
                .x(() => response.EnsureSuccessStatusCode());
        }

        [Scenario]
        public void HappyFlow()
        {
            HttpClient client = null;
            BargeService service = null;
            string repositoryPath = null;
            int exitCode = 0;
            HttpResponseMessage response = null;
            IEnumerable<Repository> repositories = null;

            "Given we have a server"
                .x(() => client = factory.CreateClient());
            "And we have a valid service protocol"
                .x(() => service = CreateService(client));
            "And a valid repository in path x"
                .x(() => repositoryPath = SetupValidPath("x"));
            
            "When I register this path"
                .x(() => exitCode = RunClient(service, $"repository register {repositoryPath}"));
            "Then the result should be 0"
                .x(() => exitCode.Should().Be(0));
            
            "When we retrieve repositories from the server"
                .x(() => response = client.GetAsync("/api/Repository").Result );
            "Then the response should be succesful"
                .x(() => response.EnsureSuccessStatusCode());
            "And the value of the response should be a list of repositories"
                .x(() => repositories = response.Content.ReadAsAsync<IEnumerable<Repository>>().Result);
            "And the result should contain a repository with repository path x"
                .x(() => repositories.FirstOrDefault(r => r.Path == repositoryPath).Should().NotBeNull());
        }

        [Scenario]
        public void ListHappyFlow()
        {
            HttpClient client = null;
            BargeService service = null;
            string repositoryPath = null;
            int exitCode = 0;
            

            "Given we have a server"
                .x(() => client = factory.CreateClient());
            "And we have a valid service protocol"
                .x(() => service = CreateService(client));
            "And a valid repository in path ./foobar/"
                .x(() => repositoryPath = SetupValidPath("./foobar/"));

            "When I register this path"
                .x(() => exitCode = RunClient(service, $"repository register {repositoryPath}"));
            "Then the result should be 0"
                .x(() => exitCode.Should().Be(0));
            
            "When I retrieve the registered repositories"
                .x(() => exitCode = RunClient(service, "repository list"));
            "Then the result should be 0"
                .x(() => exitCode.Should().Be(0));
        }
        
        private string SetupValidPath(string path){
            gitClient.Setup(m => m.GetRepositoryBasePath(It.Is<string>(s => s == path))).Returns(path);
            return path;
        }

        private BargeService CreateService(HttpClient client)
            => new BargeService(new BargeClient(client));
        private int RunClient(BargeService service, string args)
            => new Client.Runner(service, new RequestBuilder(gitClient.Object)).Run(args.Split(' '));
    }
}
