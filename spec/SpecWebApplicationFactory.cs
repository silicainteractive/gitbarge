using System.Net;
using GitBarge.Lib.Protocol;
using GitBarge.Server;
using GitBarge.Server.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace GitBarge.Specifications {
    public class SpecWebApplicationFactory : WebApplicationFactory<Startup> {
        protected override void ConfigureWebHost(IWebHostBuilder builder) {
            builder.ConfigureServices(services => {
                var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

                services.AddDbContext<BargeContext>(options => 
                {
                    options.UseInMemoryDatabase(databaseName: "Specs");
                    options.UseInternalServiceProvider(serviceProvider);
                });

                services.AddMvc().AddNewtonsoftJson();

                // Build the service provider.
                var sp = services.BuildServiceProvider();
                using (var scope = sp.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;
                    var db = scopedServices.GetRequiredService<BargeContext>();
                    db.Database.EnsureCreated();
                }
            });
        }
        
    }
    
}